import { type TextClassificationPipeline, env, pipeline, type TextClassificationOutput } from "@xenova/transformers";

export default async function classify(text: string | string[]): Promise<TextClassificationOutput | TextClassificationOutput[]>  {
	env.cacheDir = './cache'

	const classifier: TextClassificationPipeline = await pipeline("text-classification", "Xenova/distilbert-base-uncased-finetuned-sst-2-english")
	const output = classifier(text);

	return output;
}
