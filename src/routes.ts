import { FastifyInstance, FastifyPluginOptions, FastifyRequest, FastifyReply } from "fastify";
import type { TextClassificationOutput } from '@xenova/transformers';
import classifier from './Classifier.js';

/**
 * A custom type guard function that determines whether
 * `value` is an array that only contains strings.
 */
function isStringArray(value: unknown): value is string[] {
  return (
    Array.isArray(value) && value.every(element => typeof element === "string")
  );
}

function isObject(value: unknown): value is object {
  return typeof value === 'object' && value !== null;
}

async function routes(fastify: FastifyInstance, options: FastifyPluginOptions) {
  fastify.post("/classify", async (request: FastifyRequest, reply: FastifyReply) => {
    // Step 1:
    // I want to access the text property in the request body JSON, so that I can retrieve an array of strings
    // const textToClassify: string[] = request.body.text

    // Problem: 'request.body' is of type 'unknown'. ts(18046)
    // Solution: I need to type-check 'request.body' before I can use it, to confirm that I can use it how I want
    if( typeof request.body === 'object' ){
      // Step 2:
      // Confirmed that 'request.body' will be an object, now access text property
      // const textToClassify: string[] = request.body.text

      // Problem 1: 'request.body' is possibly 'null'.ts(18047)
      // Solution 1: Type-check to confirm that request.body is not null

      // Problem 2: Property 'text' does not exist on type 'object'.ts(2339)
      // Solution 2: Type-check to confirm that object request.body has property 'text
      if( request.body !== null && 'text' in request.body ){
        // Step 3:
        // const textToClassify: string[] = request.body.text    
        // Problem: Type 'unknown' is not assignable to type 'string[]'.ts(2322)
        // Solution: Confirm that request.body.text is an array of strings
        if( Array.isArray(request.body.text) && request.body.text.every(element => typeof element === "string") ){
          // Success
          const textToClassify: string[] = request.body.text;
          let output: TextClassificationOutput | TextClassificationOutput[] = await classifier(textToClassify);
          return {
            inputText: request.body.text,
            results: output
          }
        }        
      }
    }
    


    // if( isObject(request.body) ){
    //   if( 'text' in request.body && isStringArray(request.body.text) ){
    //     const textToClassify: string[] = request.body.text;
    //     let output: TextClassificationOutput | TextClassificationOutput[] = await classifier(textToClassify);
    //     return output;
    //   }
    // }
  })
}

export default routes;